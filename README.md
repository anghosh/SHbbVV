# sh4banalysis

This is a first outline for the SH->4b framework code.
The structure is similar to the HH->4b [XhhCommon](https://gitlab.cern.ch/hh4b/XhhCommon/-/tree/master).
But this code is based on rel22 (see [AnalysisBase releases](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes22pt2)), and it uses the common CP algs (see this [Tutorial](https://atlassoftwaredocs.web.cern.ch/ABtutorial/cpalg_intro/)).

Framework developers are very welcome to contribute!

## Checkout

```
setupATLAS
lsetup git
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/sh4b/sh4banalysis.git
```

## Compile

```
cd sh4banalysis
asetup 22.2.82,AnalysisBase
mkdir build
cd build
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../sources
make
```

## Run on a PHYS ttbar test sample

```
mkdir ../run
cd ../run
source ../build/x86_64-centos7-gcc11-opt/setup.sh
chmod u+x ../sources/sh4b/scripts/submit_minimalCP.sh
../sources/sh4b/scripts/submit_minimalCP.sh --mode files --mc --daodphys /afs/cern.ch/user/s/schaarsc/public/DAOD/DAOD_PHYS.27674492._000001.pool.root.1
```

When running on data22 or mc21, you must use the --isrun3 flag, eg.
```
../sources/sh4b/scripts/submit_minimalCP.sh --mode files --daodphys --isrun3 /my/dir/data22_13p6TeV...DAOD_PHYS...
```
