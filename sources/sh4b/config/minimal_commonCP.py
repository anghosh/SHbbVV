import ROOT
from xAODAnaHelpers import Config as xAH_config
#from xAODAnaHelpers.commonCPalgs import makeSequence
from sh4b.commonCPalgs import makeSequence_PHYS
from sh4b.commonCPalgs import makeSequence_PHYSLITE

import sys, os

import shlex
import argparse

c = xAH_config()

parser = argparse.ArgumentParser(description='Test for extra options')
parser.add_argument('--daodPhys',            dest='daodPhys',        action="store_true", default=False)
parser.add_argument('--daodPhyslite',        dest='daodPhyslite',    action="store_true", default=False)
parser.add_argument('--isRun3',              dest='isRun3',          action="store_true", default=False)
o = parser.parse_args(shlex.split(args.extra_options))


bstGRLList = ['GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml',
              'GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml',
              'GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml',
              'GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml']

resGRLList = ['GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml',
              'GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_BjetHLT.xml',
              'GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_BjetHLT_Normal2017.xml',
              'GoodRunsLists/data18_13TeV/20200426/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_PHYS_StandardGRL_All_Good_25ns_BjetHLT.xml']

bstLumicalcList = ['GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root',
                   'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root',
                   'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root',
                   'GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root']

resLumicalcList = ['GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root',
                   'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_BjetHLT_297730-311481_OflLumi-13TeV-009.root',
                   'GoodRunsLists/data17_13TeV/20180619/physics_25ns_BjetHLT_Normal2017.lumicalc.OflLumi-13TeV-010.root',
                   'GoodRunsLists/data18_13TeV/20200426/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010-2.root']

bstGRL    = ",".join(bstGRLList)
resGRL    = ",".join(resGRLList)
lumicalcs = ",".join(bstLumicalcList)
actualMu2017File = 'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'
actualMu2018File = 'GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'

trigChains = [
    #bjet triggers:
    #2018:
    'HLT_j175_gsc225_bmv2c1040_split',
    'HLT_j225_gsc275_bmv2c1060_split',
    'HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split',
    'HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30',
    'HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25',
    'HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21',
    #2017:
    'HLT_j175_gsc225_bmv2c1040_split',
    'HLT_j225_gsc275_bmv2c1060_split',
    'HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split',
    'HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30',
    'HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25',
    'HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21',
    'HLT_2j35_gsc55_bmv2c1060_split_ht300_L1HT190-J15s5.ETA21',
    #2016:
    'HLT_j175_bmv2c2040_split',
    'HLT_j225_bmv2c2060_split',
    'HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25',
    'HLT_2j55_bmv2c2060_split_ht300_L14J15',
    'HLT_j100_2j55_bmv2c2060_split',
    'HLT_j150_bmv2c2060_split_j50_bmv2c2060_split',
    #large-R triggers:
    'HLT_j340','HLT_j360_*','HLT_j380','HLT_j390_*','HLT_j400_*','HLT_j420_*','HLT_j440_*','HLT_j460_*','HLT_j480_*',
    'HLT_2j330_*','HLT_j360_*_j360_*','HLT_j370_*_j370_*','HLT_j260_320eta490',
    #data22:
    'HLT_j360_a10t_lcw_jes_L1J100',
    'HLT_j360_a10t_lcw_jes_L1SC111-CJ15',
    'HLT_j420_a10sd_cssk_pf_jes_ftf_preselj225_L1J100',
    'HLT_j225_0eta290_020jvt_bdl1d70_pf_ftf_preselj180_L1J100',
    'HLT_j300_0eta290_020jvt_bdl1d77_pf_ftf_preselj225_L1J100',
    'HLT_j360_0eta290_020jvt_bdl1d85_pf_ftf_preselj225_L1J100',
    'HLT_j175_0eta290_020jvt_bdl1d60_j60_0eta290_020jvt_bdl1d60_pf_ftf_preselj140b85XXj45b85_L1J100',
    'HLT_j150_2j55_0eta290_020jvt_bdl1d70_pf_ftf_preselj80XX2j45b90_L1J85_3J30',
    'HLT_3j65_0eta290_020jvt_bdl1d77_pf_ftf_presel3j45b95_L13J35p0ETA23',
    'HLT_j75_0eta290_020jvt_bdl1d60_3j75_pf_ftf_preselj50b85XX3j50_L14J20',
    'HLT_2j35c_020jvt_bdl1d60_2j35c_020jvt_pf_ftf_presel2j25XX2j25b85_L14J15p0ETA25',
    'HLT_2j45_0eta290_020jvt_bdl1d60_2j45_pf_ftf_presel2j25XX2j25b85_L14J15p0ETA25',
    'HLT_2j45_0eta290_020jvt_bdl1d70_j0_HT300_j0_DJMASS700j35_pf_ftf_L1HT150',
    'HLT_3j35_0eta290_020jvt_bdl1d70_j35_pf_ftf_presel2j25XX2j25b85_L14J15p0ETA25',
    'HLT_2j35_0eta290_020jvt_bdl1d70_2j35_0eta290_020jvt_bdl1d85_pf_ftf_presel4j25b95_L14J15p0ETA25',
    'HLT_4j35_0eta290_020jvt_bdl1d77_pf_ftf_presel4j25b95_L14J15p0ETA25',
    'HLT_2j35_0eta290_020jvt_bdl1d60_3j35_pf_ftf_presel3j25XX2j25b85_L15J15p0ETA25'
    
    
]


dataType="data"
if args.is_MC:
	dataType="mc"

"""
#will remove this completely soon. PU rw is done by the common CP algs. PV cut is moved to EventSelection.

c.algorithm("BasicEventSelection", { "m_name"                  : "basicEventSel",
                                     "m_derivationName"        : "", #if o.daodPhys else "EXOT8Kernel",
                                     "m_applyGRLCut"           : not args.is_MC,
                                     "m_doPUreweighting"       : False, #args.is_MC if o.daodPhys else False,
                                     "m_lumiCalcFileNames"     : lumicalcs,
                                     "m_autoconfigPRW"         : True,
                                     "m_prwActualMu2017File"   : actualMu2017File,
                                     "m_prwActualMu2018File"   : actualMu2018File,
                                    # "m_PRWFileNames"          : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID410xxx/pileup_mc16e_dsid410080_FS.root",
                                    # "m_GRLxml"                : bstGRL,
                                     "m_PVNTrack"              : 2,
                                     "m_applyPrimaryVertexCut" : False,
                                     "m_applyEventCleaningCut" : False,
                                     "m_applyJetCleaningEventFlag" : False,
                                     "m_applyCoreFlagsCut"     : False,
                                     "m_useMetaData"           : True
                                     } )
"""

isRun3=False
if o.isRun3:
    isRun3=True

if o.daodPhys:
    c.algorithm(makeSequence_PHYS(dataType,trigChains,isRun3))

if o.daodPhyslite:
    c.algorithm(makeSequence_PHYSLITE(dataType,trigChains,isRun3))

c.algorithm("EventSelection", { "m_name"               : "EventSelection",
                                "m_outputStream"       : "Analysis_EvtSelection",
                                "m_muonContainerName"  : "AnalysisMuons_%SYS%",
                                "m_muonPtCut"          : 10e3,
                                "m_muonEtaCut"         : 2.5,
                                "m_reco4JetName"       : "AnalysisJetsBTAG_%SYS%",
                                "m_reco10JetName"      : "AnalysisLargeRRecoJets_%SYS%",
                                "m_smallRjetPtCut"     : 25e3,
                                "m_smallRjetNumberCut" : 0,
                                "m_smallRbjetNumberCut": 4,
                                "m_smallRjetEtaCut"    : 2.5,
                                "m_largeRjetNumberCut" : 0,
                                "m_largeRjetPtCut"     : 250e3,
                                "m_largeRjetTrackName" : "GhostAntiKtVR30Rmax4Rmin02PV0TrackJets",
                                "m_trackJetPtCut"      : 5e3,
                                "m_trackJetEtaCut"     : 2.5,
                                "m_trigStudy"          : False,
                                "m_triggerChains"      : trigChains,
                                "m_isPhyslite"         : True if o.daodPhyslite else False
                               } )

c.algorithm("SH4BTree", { "m_name"                   : "SH4BTree",
                          "m_outputStream"           : "Analysis_SH4B",
                          "m_muonContainerName"      : "SelectedAnalysisMuons",
                          "m_muonDetailStr"          : "kinematic quality",
                          "m_truth4JetName"          : "AntiKt4TruthDressedWZJets",
                          "m_truth10JetName"         : "AntiKt10TruthTrimmedPtFrac5SmallR20Jets",
                          "m_reco4JetName"           : "SelectedAnalysisJetsBTAG_%SYS%",
                          "m_reco10JetContainerName" : "SelectedAnalysisLargeRRecoJets_%SYS%",
                          "m_reco10JetDetailStr"     : "kinematic substructure constituent",
                          "m_reco10JetName"          : "recojet_antikt10",
                          "m_isPhyslite"             : True if o.daodPhyslite else False
                     } )
