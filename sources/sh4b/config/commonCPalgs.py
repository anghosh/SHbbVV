#from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import createService
import ROOT

def makeSequence_PHYS (dataType, trigChains, isRun3) :

    print("Now preparing CP alg PHYS sequence for datatype ",dataType)
    muonContainer='Muons'
    jetContainer = 'AntiKt4EMPFlowJets'
    largeRrecojetContainer = 'AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets'
    metContainerBasename="AntiKt4EMPFlow"
    
    print("CHECK trigChains ",trigChains);
    
    algSeq = AnaAlgSequence()
    
    sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = algSeq )
    sysService.systematicsList= ['NOSYS']
    #sysService.sigmaRecommended = 1
    
    from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
    pileupSequence = makePileupAnalysisSequence( dataType )
    pileupSequence.configure( inputName = 'EventInfo', outputName = 'EventInfo_%SYS%' )
    algSeq += pileupSequence
    
    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    muonSequenceLoose = makeMuonAnalysisSequence( dataType, deepCopyOutput = True, shallowViewOutput = False,
                                                   workingPoint = 'Loose.NonIso', postfix = 'loose' )
    muonSequenceLoose.configure( inputName = muonContainer, outputName = 'AnalysisMuons_%SYS%' )
    algSeq += muonSequenceLoose
    if isRun3:
        algSeq.MuonAnalysisSequence_loose.MuonSelectionAlg_loose.selectionTool.IsRun3Geo = True
    
    from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
    jetSequence = makeJetAnalysisSequence( dataType, jetContainer, postfix="smallR", deepCopyOutput = True,
                                           shallowViewOutput = False, runGhostMuonAssociation = False,
                                           runFJvtUpdate = False, runFJvtSelection = False, runJvtSelection = False)
    from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence
    ftagSequence = makeFTagAnalysisSequence( jetSequence, dataType, jetContainer,
                              btagWP = "FixedCutBEff_77", btagger = "DL1dv00", postfix = "",
                              preselection=None, kinematicSelection = False, noEfficiency = False,
                              legacyRecommendations = False, enableCutflow = False, minPt = 20000 )    
    jetSequence.configure( inputName = jetContainer, outputName = 'AnalysisJetsBTAG_%SYS%')
    #print( jetSequence ) # For debugging
    algSeq += jetSequence
    
    #cdi files are here: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/
    algSeq.JetAnalysisSequence_smallR.FTagSelectionAlgDL1dv00FixedCutBEff_77.selectionTool.FlvTagCutDefinitionsFileName="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    if dataType=="mc":
        algSeq.JetAnalysisSequence_smallR.FTagEfficiencyScaleFactorAlgDL1dv00FixedCutBEff_77.efficiencyTool.ScaleFactorFileName ="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    
    VRtrackjetContainer = 'AntiKtVR30Rmax4Rmin02PV0TrackJets'
    VRftagSequence = AnaAlgSequence("FTagAnalysisSequence")
    #the following lines make a shallow copy, this isn't needed anymore with this MR https://gitlab.cern.ch/atlas/athena/-/merge_requests/54929
    #alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg', 'VRJetSelectionAlg')
    #VRftagSequence.append( alg, inputPropName = 'input', outputPropName = 'output',
    #                       stageName = 'selection', dynConfig = {} )
    makeFTagAnalysisSequence( VRftagSequence, dataType, 'AntiKtVR30Rmax4Rmin02TrackJets', btagWP = "FixedCutBEff_77",
                              btagger = "DL1r", postfix = "VR", preselection=None, kinematicSelection = True,
                              noEfficiency = False, legacyRecommendations = False, enableCutflow = False, minPt = 20000 )
    VRftagSequence.configure( inputName = VRtrackjetContainer, outputName = 'VRTrackJetsBTAG_%SYS%' )
    algSeq += VRftagSequence
    
    algSeq.FTagAnalysisSequence.FTagSelectionAlgDL1rFixedCutBEff_77VR.selectionTool.FlvTagCutDefinitionsFileName="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    if dataType=="mc":
        algSeq.FTagAnalysisSequence.FTagEfficiencyScaleFactorAlgDL1rFixedCutBEff_77VR.efficiencyTool.ScaleFactorFileName ="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    
    largeRrecojetSequence = makeJetAnalysisSequence( dataType, largeRrecojetContainer, postfix="largeR",
                                                     deepCopyOutput = True, shallowViewOutput = False,
                                                     runGhostMuonAssociation = False, largeRMass="Calo")
    largeRrecojetSequence.configure( inputName = largeRrecojetContainer, outputName = 'AnalysisLargeRRecoJets_%SYS%')
    print( largeRrecojetSequence ) # For debugging
    algSeq += largeRrecojetSequence

    from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence
    metSequence = makeMetAnalysisSequence( dataType, metSuffix = metContainerBasename)
    metSequence.configure( inputName = { 'jets'      : 'AnalysisJetsBTAG_%SYS%',
                                         'muons'     : 'AnalysisMuons_%SYS%'   },
                           outputName = 'AnalysisMET_%SYS%' )
    algSeq += metSequence
 
    from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
    #trigSequence = makeTriggerAnalysisSequence( dataType, triggerChains=trigChains, prescaleLumiCalcFiles='lumicalcs' )
    trigSequence = makeTriggerAnalysisSequence( dataType, triggerChains=trigChains)
    algSeq += trigSequence

    print ( algSeq ) # For debugging
    
    return algSeq


def makeSequence_PHYSLITE (dataType, trigChains, isRun3) :

    print("Now preparing CP alg PHYSLITE sequence for datatype ",dataType)

    muonContainer='AnalysisMuons'
    jetContainer = 'AnalysisJets'
    metContainerBasename="AnalysisMET"

    algSeq = AnaAlgSequence()
    
    sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = algSeq )
    sysService.systematicsList= ['NOSYS']
    #sysService.sigmaRecommended = 1
    
    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    muonSequenceLoose = makeMuonAnalysisSequence( dataType, deepCopyOutput = True, shallowViewOutput = False,
                                                   workingPoint = 'Loose.NonIso', postfix = 'loose' )
    muonSequenceLoose.configure( inputName = muonContainer, outputName = 'AnalysisMuons_%SYS%' )
    algSeq += muonSequenceLoose
    if isRun3:
        algSeq.MuonAnalysisSequence_loose.MuonSelectionAlg_loose.selectionTool.IsRun3Geo = True
    
    #note: PHYSLITE currently doesn't support DL1dv00, to be fixed
    from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
    jetSequence = makeJetAnalysisSequence( dataType, jetContainer, postfix="smallR", deepCopyOutput = True, shallowViewOutput = False, runGhostMuonAssociation = False, runFJvtUpdate = False, runFJvtSelection = False, runJvtSelection = False)
    from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence
    ftagSequence = makeFTagAnalysisSequence( jetSequence, dataType, 'AntiKt4EMPFlowJets',
                              btagWP = "FixedCutBEff_77", btagger = "DL1r", postfix = "",
                              preselection=None, kinematicSelection = False, noEfficiency = False,
                              legacyRecommendations = False, enableCutflow = False, minPt = 20000 )
    jetSequence.configure( inputName = jetContainer, outputName = 'AnalysisJetsBTAG_%SYS%')
    #print( jetSequence ) # For debugging
    algSeq += jetSequence

    #cdi files are here: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/
    algSeq.JetAnalysisSequence_smallR.FTagSelectionAlgDL1rFixedCutBEff_77.selectionTool.FlvTagCutDefinitionsFileName="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    if dataType=='mc':
        algSeq.JetAnalysisSequence_smallR.FTagEfficiencyScaleFactorAlgDL1rFixedCutBEff_77.efficiencyTool.ScaleFactorFileName ="xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"
    
    from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence
    metSequence = makeMetAnalysisSequence( dataType, metSuffix = metContainerBasename)
    metSequence.configure( inputName = { 'jets'      : 'AnalysisJetsBTAG_%SYS%',
                                         'muons'     : 'AnalysisMuons_%SYS%'    },
                           outputName = 'AnalysisMET_%SYS%' )
    algSeq += metSequence
    
    from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
    #trigSequence = makeTriggerAnalysisSequence( dataType, triggerChains=trigChains, prescaleLumiCalcFiles='lumicalcs' )
    trigSequence = makeTriggerAnalysisSequence( dataType, triggerChains=trigChains)
    algSeq += trigSequence

    print ( algSeq ) # For debugging
    
    return algSeq

