#ifndef sh4b_EventSelection_H
#define sh4b_EventSelection_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include "xAODAnaHelpers/Algorithm.h"
#include <AnaAlgorithm/AnaAlgorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODMuon/MuonContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODCore/AuxContainerBase.h"
#include "xAODParticleEvent/Particle.h"

#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

class EventSelection : public EL::AnaAlgorithm {

public:

  xAOD::TEvent *m_event = nullptr;               //!
  xAOD::TStore *m_store = nullptr;               //!

  bool m_debug;
  std::string m_muonContainerName;
  std::string m_reco4JetName; 
  std::string m_reco10JetName;
  bool m_isPhyslite;
  
  double m_muonPtCut;
  double m_muonEtaCut;
  double m_smallRjetPtCut;
  double m_smallRjetEtaCut;
  double m_largeRjetPtCut;
  std::string m_largeRjetTrackName;
  float m_trackJetPtCut;
  float m_trackJetEtaCut;
 
  long unsigned int m_smallRjetNumberCut;
  long unsigned int m_smallRbjetNumberCut;
  long unsigned int m_largeRjetNumberCut;

  bool m_trigPass;
  bool m_trigStudy;

  std::vector<std::string> m_triggerChains;
  
  // this is a standard algorithm constructor
  EventSelection (const std::string& name, ISvcLocator* pSvcLocator);
  ~EventSelection() {};
  
  CP::SysListHandle m_systematicsList {this};
  
  CP::SysReadHandle<xAOD::MuonContainer> m_muonHandle {this, "muons", "AnalysisMuons_%SYS%", "the muon collection to run on"};
  CP::SysWriteHandle<xAOD::MuonContainer, xAOD::AuxContainerBase> m_muonWriteHandle {this, "muonsOut", "SelectedAnalysisMuons_%SYS%", "the muon collection to write out"};
  
  CP::SysReadHandle<xAOD::JetContainer> m_smallRJetHandle {this, "smallRJets", "AnalysisJetsBTAG_%SYS%", "the jet collection to run on"};
  CP::SysWriteHandle<xAOD::JetContainer,xAOD::AuxContainerBase> m_smallRJetWriteHandle {this, "smallRJetsOut", "SelectedAnalysisJetsBTAG_%SYS%", "the jet collection to write out"};
  
  CP::SysReadHandle<xAOD::JetContainer> m_largeRJetHandle {this, "largeRJets", "AnalysisLargeRRecoJets_%SYS%", "the jet collection to run on"};
  CP::SysWriteHandle<xAOD::JetContainer,xAOD::AuxContainerBase> m_largeRJetWriteHandle {this, "largeRJetsOut", "SelectedAnalysisLargeRRecoJets_%SYS%", "the jet collection to write out"};

  asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdec;
  
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;              
  virtual StatusCode execute () override;             
  virtual StatusCode finalize () override; 


  /// output variables for the current event
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

private:
  std::string m_name;
  std::string m_outputStream;
  
  static bool SortPt(const xAOD::IParticle* j1, const xAOD::IParticle* j2){return j1->pt() > j2->pt();}
  void ApplyMuonInSmallRJetCorrection(xAOD::Jet* jet, const xAOD::MuonContainer* muons) const;
  void ApplyMuonInLargeRJetCorrection(xAOD::Jet* fatJet, const xAOD::MuonContainer* muons,
                                  std::vector<const xAOD::Jet*> trackJets) const;

};

#endif

