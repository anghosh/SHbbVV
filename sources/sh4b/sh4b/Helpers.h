#ifndef sh4b_Helpers_H
#define sh4b_Helpers_H

#include "TString.h"

class Helpers
{

public:
  
  static bool check_sys(std::string type, std::string sys)
  {
   TString sysname=sys;
   bool check=false;
   
   if(type=="EventInfo")
   {
    check=true;
   }
   
   if(type=="Truth")
   {
    if(sys=="NOSYS")
     check=true;
   }
   
   if(type=="VRtrackjets")
   {
    if(sys=="NOSYS")
     check=true;
   }
      if(type=="Muons")
   {
    if(sys=="NOSYS" || sysname.Contains("MUON_"))
     check=true;
   }

   if(type=="smallR")
   {
    if(sys=="NOSYS" || sysname.Contains("JET_BJES_") || sysname.Contains("JET_Comb_") || sysname.Contains("JET_EffectiveNP_")  || sysname.Contains("JET_EtaIntercalibration_") || sysname.Contains("JET_Flavor_") || sysname.Contains("JET_JER_") || sysname.Contains("JET_Pileup_") || sysname.Contains("JET_PunchThrough_") || sysname.Contains("JET_SingleParticle_"))
     check=true;
   }
   
   if(type=="largeR")
   {
    if(sys=="NOSYS" || sysname.Contains("JET_Rtrk_") || sysname.Contains("JET_MassRes_"))
     check=true;
   }
   
   if(type=="MET")
   {
    check=true;
   }

   return check;
  };
  

};

#endif

