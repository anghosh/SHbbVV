#ifndef SH4BTree_SH4BTree_H
#define SH4BTree_SH4BTree_H

#include <EventLoop/StatusCode.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "TTree.h"
#include "TH1D.h"

#include "xAODParticleEvent/Particle.h"
#include <xAODMuon/MuonContainer.h>
#include <xAODMissingET/MissingETContainer.h>

// external tools include(s):
#include "AsgTools/AnaToolHandle.h"

#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODAnaHelpers/HelpTreeBase.h>
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"

class HelpTreeBase;

class SH4BTree : public EL::AnaAlgorithm
{
 
 private: 
 
 std::string m_name;
 std::string m_outputStream;
 bool m_isPhyslite;
 class IBTaggingEfficiencyTool;

 asg::AnaToolHandle<IBTaggingSelectionTool> btagSelTool;

 public:
 
 // this is a standard algorithm constructor
 SH4BTree (const std::string& name, ISvcLocator* pSvcLocator);
 ~SH4BTree() {};
 
 CP::SysListHandle m_systematicsList {this};
 CP::SysReadHandle<xAOD::MuonContainer> m_muonHandle {this, "muons", "SelectedAnalysisMuons_%SYS%", "the muon collection to run on"};
 CP::SysReadHandle<xAOD::JetContainer> m_jetsmallRHandle {this, "jetsmallR", "SelectedAnalysisJetsBTAG_%SYS%", "the small-R jet collection to run on"};
 //CP::SysReadHandle<xAOD::JetContainer> m_jetsmallRHandle {this, "jetsmallR", m_reco4JetName, "the small-R jet collection to run on"};
 CP::SysReadHandle<xAOD::JetContainer> m_jetlargeRHandle {this, "jetlargeR", "SelectedAnalysisLargeRRecoJets_%SYS%", "the large-R jet collection to run on"};
 CP::SysReadHandle<xAOD::MissingETContainer> m_metHandle {this, "met", "AnalysisMET_%SYS%", "the MET collection to run on"};
 CP::SysReadHandle<xAOD::JetContainer> m_VRtrackjetHandle {this, "VRtrackjets", "VRTrackJetsBTAG_%SYS%", "the VR track jet collection to run on"};

 bool m_debug;
 bool m_isMC;
 
 std::string m_evtDetailStr;
 std::string m_muonContainerName;
 std::string m_muonDetailStr;
 std::string m_metContainerName;
 std::string m_metDetailStr;
 std::string m_resolvedJetsName;
 std::string m_resolvedJetDetailStr;
 std::string m_reco10JetDetailStr;
 std::string m_reco10JetName;

 std::string m_truth4JetName;
 std::string m_truth10JetName;
 std::string m_reco4JetName;
 std::string m_reco10JetContainerName;
 std::string m_truthParticleContainerName;
 	 
 // these are the functions inherited from Algorithm
 virtual StatusCode initialize () override;
 virtual StatusCode execute () override;
 virtual StatusCode finalize () override;  
 
 virtual StatusCode AddTree (std::string);      //!
 
 /** The TEvent object */
 xAOD::TEvent* m_event = nullptr; //!
 /** The TStore object */
 xAOD::TStore* m_store = nullptr; //!

 private:
 
 std::map< std::string, HelpTreeBase* > m_helpTree; //!

 // momentum vectors for jet -- //
 std::vector<float>  truthjet_antikt4_pt;
 std::vector<float>  truthjet_antikt4_eta;
 std::vector<float>  truthjet_antikt4_phi;
 std::vector<float>  truthjet_antikt4_m;

 std::vector<float>  truthjet_antikt10_pt;
 std::vector<float>  truthjet_antikt10_eta;
 std::vector<float>  truthjet_antikt10_phi;
 std::vector<float>  truthjet_antikt10_m;

 std::vector<long unsigned int> nrecojet_antikt4;
 std::vector<float>  recojet_antikt4_pt;
 std::vector<float>  recojet_antikt4_eta;
 std::vector<float>  recojet_antikt4_phi;
 std::vector<float>  recojet_antikt4_m;
 std::vector<bool>   recojet_antikt4_btag77;
 std::vector<long unsigned int> nrecojet_antikt4_btag77;
 std::vector<double> recojet_antikt4_btagscore;
 
 std::vector<float>  VRtrackjet_pt;
 std::vector<float>  VRtrackjet_eta;
 std::vector<float>  VRtrackjet_phi;
 std::vector<float>  VRtrackjet_m;
 std::vector<bool>   VRtrackjet_btag77;

 std::vector<float> met_pt;
 std::vector<float> met_px;
 std::vector<float> met_py;
 std::vector<float> met_sumet;
 std::vector<float> met_phi;
 
 
 std::vector<float> truth_b_fromH_pt;
 std::vector<float> truth_b_fromH_eta;
 std::vector<float> truth_b_fromH_phi;
 std::vector<float> truth_b_fromH_m;
 std::vector<float> truth_b_fromS_pt;
 std::vector<float> truth_b_fromS_eta;
 std::vector<float> truth_b_fromS_phi;
 std::vector<float> truth_b_fromS_m;

 float truth_H_pt  = 0;
 float truth_H_eta = 0;
 float truth_H_phi = 0;
 float truth_H_m   = 0;
 float truth_S_pt  = 0;
 float truth_S_eta = 0;
 float truth_S_phi = 0;
 float truth_S_m   = 0;
  
 // this is needed to distribute the algorithm to the workers
 ClassDef(SH4BTree, 1);                                 //!
 
};


#endif
