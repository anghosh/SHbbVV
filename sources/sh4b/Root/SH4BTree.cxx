//#include <AsgTools/MessageCheck.h>
#include <sh4b/SH4BTree.h>
#include <sh4b/Helpers.h>
#include <xAODEventInfo/EventInfo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/AuxContainerBase.h"


using std::cout;  using std::endl;
using std::string; using std::vector;

#include "TH1F.h"

#include <xAODAnaHelpers/HelpTreeBase.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>
//#include <xAODMissingET/MissingETContainer.h>

// this is needed to distribute the algorithm to the workers
ClassImp(SH4BTree)

SH4BTree :: SH4BTree (const std::string& name,ISvcLocator *pSvcLocator) : EL::AnaAlgorithm (name, pSvcLocator)
{
 // Here you put any code for the base initialization of variables,
 // e.g. initialize all pointers to 0.  This is also where you
 // declare all properties for your algorithm.  Note that things like
 // resetting statistics variables or booking histograms should
 // rather go into the initialize() function.
 
 declareProperty("m_name", m_name = "SH4BTree");
 declareProperty("m_outputStream", m_name = "Analysis2");
 declareProperty("m_muonContainerName", m_muonContainerName = "SelectedAnalysisMuons");
 declareProperty("m_truth4JetName",  m_truth4JetName = "AntiKt4TruthDressedWZJets");
 declareProperty("m_truth10JetName", m_truth10JetName = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets");
 declareProperty("m_reco4JetName",   m_reco4JetName = "SelectedAnalysisJetsBTAG");
 declareProperty("m_reco10JetContainerName",  m_reco10JetContainerName = "SelectedAnalysisLargeRRecoJets");
 declareProperty("m_muonDetailStr",  m_muonDetailStr = "");
 declareProperty("m_reco10JetDetailStr",  m_reco10JetDetailStr = "");
 declareProperty("m_reco10JetName",  m_reco10JetName = "");
 declareProperty("m_truthParticleContainerName",  m_truthParticleContainerName = "TruthBSMWithDecayParticles");
 declareProperty("m_isPhyslite", m_isPhyslite=false);
}



StatusCode SH4BTree :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
 ANA_MSG_INFO ("in initialize");
 
 m_event = wk()->xaodEvent();
 m_store = wk()->xaodStore();
 
 m_debug=false;
 
 if(m_debug) Info("initialize()", "after add store");
 
 m_systematicsList.addHandle(m_muonHandle);
 m_systematicsList.addHandle(m_jetsmallRHandle);
 m_systematicsList.addHandle(m_jetlargeRHandle);
 m_systematicsList.addHandle(m_metHandle);
 m_systematicsList.addHandle(m_VRtrackjetHandle);
 ANA_CHECK (m_systematicsList.initialize());

 // FIXME all names are hard-coded for testing purposes
 btagSelTool.setTypeAndName("BTaggingSelectionTool/btagSelTool");
  ANA_CHECK (btagSelTool.setProperty( "FlvTagCutDefinitionsFileName","xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root" ));
  ANA_CHECK (btagSelTool.setProperty("TaggerName",     "DL1dv00" ));
  ANA_CHECK (btagSelTool.setProperty("OperatingPoint", "FixedCutBEff_77"));
  ATH_CHECK (btagSelTool.setProperty("MinPt",          25 ) );
  ANA_CHECK (btagSelTool.setProperty("JetAuthor",      "AntiKt4EMPFlowJets" ));
  ANA_CHECK (btagSelTool.initialize());

 if(m_debug) Info("initialize()", "left");
 
 return StatusCode::SUCCESS;
}



StatusCode SH4BTree :: execute ()
{
 // Here you do everything that needs to be done on every single
 // events, e.g. read input variables, apply cuts, and fill
 // histograms and trees.  This is where most of your actual analysis
 // code will go.
 
 // retrieve the eventInfo object from the event store
 const xAOD::EventInfo *eventInfo = nullptr;
 ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
 m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
 
 // print out run and event number from retrieved object
 //ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  
 for (const auto& sys : m_systematicsList.systematicsVector())
 {
  
  std::string sysname;
  ANA_CHECK(m_systematicsList.service().makeSystematicsName(sysname, "%SYS%",sys))
  
  if( m_helpTree.find( sysname ) == m_helpTree.end() )
  {
   AddTree( sysname );
  }
  
  const xAOD::EventInfo* eventInfo(0);
  ANA_CHECK( HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  const xAOD::VertexContainer* vertices(0);
  ANA_CHECK( HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
  
  const xAOD::Vertex *pv = 0;
  pv = vertices->at( HelperFunctions::getPrimaryVertexLocation( vertices ) );
  
  //std::cout<<"run_number: "<<eventInfo->runNumber()<<" event_number "<<eventInfo->eventNumber()<<std::endl;
  
  if(Helpers::check_sys("EventInfo",sysname))
   m_helpTree[sysname]->FillEvent( eventInfo );
  
  if(!m_muonContainerName.empty() && Helpers::check_sys("Muons",sysname))
  {
    if(m_debug) cout << " Filling muons " << endl;
    string muonContainerName=m_muonContainerName;
    const xAOD::MuonContainer* muons(nullptr);
    ANA_CHECK (m_muonHandle.retrieve (muons, sys));
    m_helpTree[sysname]->FillMuons(muons, HelperFunctions::getPrimaryVertex( vertices ) );
  }
  
  // --- filling the Akt4RecoJets --- //
  
  //hardcode btagging accessor: (temporary, PHYSLITE only supports DL1r for now, needs fixing asap!)
  string tagger="ftag_select_DL1dv00_FixedCutBEff_77";
  if(m_isPhyslite) 
   tagger="ftag_select_DL1r_FixedCutBEff_77";
  
  if(!m_reco4JetName.empty() && Helpers::check_sys("smallR",sysname))
  {
    nrecojet_antikt4.clear();
    recojet_antikt4_pt.clear();
    recojet_antikt4_eta.clear();
    recojet_antikt4_phi.clear();
    recojet_antikt4_m.clear();
    recojet_antikt4_btag77.clear();
    nrecojet_antikt4_btag77.clear();
    recojet_antikt4_btagscore.clear();
    
    const xAOD::JetContainer* AntiKt4RecoJets(nullptr);
    ANA_CHECK (m_jetsmallRHandle.retrieve (AntiKt4RecoJets, sys));

    for(auto jet : *AntiKt4RecoJets)
    {
      nrecojet_antikt4.push_back(AntiKt4RecoJets->size());
      recojet_antikt4_pt.push_back(jet->pt());
      recojet_antikt4_eta.push_back(jet->eta());
      recojet_antikt4_phi.push_back(jet->phi());
      recojet_antikt4_m.push_back(jet->m());
      
      SG::AuxElement::ConstAccessor<char> isTag77(tagger);
      bool pass_btag=false;
      if(isTag77.isAvailable(*jet))
      {
        recojet_antikt4_btag77.push_back(isTag77(*jet));
	      double tagweight = 0.;
	      ANA_CHECK( btagSelTool->getTaggerWeight(*jet, tagweight, false/*useVetoWP=false*/) );
	      std::cout << " recojet_antikt4_btagscore " << tagweight << std::endl;
        recojet_antikt4_btagscore.push_back(tagweight);
      }
    }
  }
  
  // --- filling the Akt10RecoJets --- //
  if(!m_reco10JetContainerName.empty() && Helpers::check_sys("largeR",sysname) && !m_isPhyslite)
  {
    const xAOD::JetContainer* AntiKt10RecoJets(nullptr);
    ANA_CHECK (m_jetlargeRHandle.retrieve (AntiKt10RecoJets, sys));
    m_helpTree[sysname]->FillFatJets(AntiKt10RecoJets, HelperFunctions::getPrimaryVertexLocation(vertices), m_reco10JetName);
  }
  
  if(Helpers::check_sys("VRtrackjets",sysname) && !m_isPhyslite)
  {
   VRtrackjet_pt.clear();
   VRtrackjet_eta.clear();
   VRtrackjet_phi.clear();
   VRtrackjet_m.clear();
   VRtrackjet_btag77.clear();
   
   const xAOD::JetContainer* VRTrackJets(0);
   ANA_CHECK(m_VRtrackjetHandle.retrieve(VRTrackJets, sys));
   for(auto jet : *VRTrackJets)
   {
    VRtrackjet_pt.push_back(jet->pt()/1000.0);
    VRtrackjet_eta.push_back(jet->eta());
    VRtrackjet_phi.push_back(jet->phi());
    VRtrackjet_m.push_back(jet->m()/1000.0);
    
    SG::AuxElement::ConstAccessor<char> isTag77("ftag_select_DL1r_FixedCutBEff_77");
    bool pass_btag=false;
    if(isTag77.isAvailable(*jet))
     VRtrackjet_btag77.push_back(isTag77(*jet));
   }
  }
  
  if(Helpers::check_sys("Truth",sysname))
  {
    
   // --- filling the Akt4TruthJets --- //
   if(!m_truth4JetName.empty() && m_isMC) {
    truthjet_antikt4_pt.clear();
    truthjet_antikt4_eta.clear();
    truthjet_antikt4_phi.clear();
    truthjet_antikt4_m.clear();

    const xAOD::JetContainer* AntiKt4TruthJets(0);
    ANA_CHECK( HelperFunctions::retrieve(AntiKt4TruthJets, m_truth4JetName, m_event, m_store));
    for(auto jet : *AntiKt4TruthJets){
      truthjet_antikt4_pt.push_back(jet->pt()/1000.0);
      truthjet_antikt4_eta.push_back(jet->eta());
      truthjet_antikt4_phi.push_back(jet->phi());
      truthjet_antikt4_m.push_back(jet->m()/1000.0);
    }
  }

   // --- filling the Akt10TruthJets --- //
   if(!m_truth10JetName.empty() && m_isMC  && !m_isPhyslite) {
    truthjet_antikt10_pt.clear();
    truthjet_antikt10_eta.clear();
    truthjet_antikt10_phi.clear();
    truthjet_antikt10_m.clear();
    const xAOD::JetContainer* AntiKt10TruthJets(0);
    ANA_CHECK( HelperFunctions::retrieve(AntiKt10TruthJets, m_truth10JetName, m_event, m_store));
    for(auto jet : *AntiKt10TruthJets){
    //  const xAOD::JetFourMom_t caloP4 = jet->jetP4("JetJMSScaleMomentumCalo");
      truthjet_antikt10_pt.push_back(jet->pt()/1000.0);
      truthjet_antikt10_eta.push_back(jet->eta());
      truthjet_antikt10_phi.push_back(jet->phi());
      truthjet_antikt10_m.push_back(jet->m()/1000.0);
    //  truthjet_antikt10_pt.push_back(caloP4.Pt());
    //  truthjet_antikt10_eta.push_back(caloP4.Eta());
    //  truthjet_antikt10_phi.push_back(caloP4.Phi());
    //  truthjet_antikt10_m.push_back(caloP4.M());
      // float tau3, tau2;
      // jet->getAttribute("Tau3_wta", tau3);
      // jet->getAttribute("Tau2_wta", tau2);
      // std::cout<<"Jet tau32 = "<< tau3/tau2 << std::endl;
    }
   }
   
    if(!m_truthParticleContainerName.empty() && m_isMC) {
      truth_b_fromH_pt.clear();
      truth_b_fromH_eta.clear();
      truth_b_fromH_phi.clear();
      truth_b_fromH_m.clear();
      truth_b_fromS_pt.clear();
      truth_b_fromS_eta.clear();
      truth_b_fromS_phi.clear();
      truth_b_fromS_m.clear();
      const xAOD::TruthParticleContainer* TruthParticles(0);
      ANA_CHECK( HelperFunctions::retrieve(TruthParticles, m_truthParticleContainerName, m_event, m_store));
      for(auto *ptcl : *TruthParticles){
        // Keep only the particles involved in the decay
        if(fabs(ptcl->pdgId()!=25) && fabs(ptcl->pdgId()!=35) && fabs(ptcl->pdgId()!=36)) continue;
        
        // Require that they have exactly 2 children and exactly one parent
        int npar=ptcl->nParents();
        int nchild=ptcl->nChildren();
        if(npar!=1) continue; 
        if(nchild!=2) continue;
        printf(" %d parent and %d children\n",npar,nchild);

        // Truth information needed only for X, S or H
        if(fabs(ptcl->pdgId()==25) || fabs(ptcl->pdgId()==35)){
          if(ptcl->parent(0)->pdgId() == 36){
	        // Found H or S from X->SH
	          printf("Truth part. ID:%5d, status: %2d, pt: %5f, eta: %5f, phi: %5f, mass: %5f\n",
	            ptcl->pdgId(),ptcl->status(),ptcl->pt(),ptcl->eta(),ptcl->phi(),ptcl->m());
	          // Save kinematics of S and H
            if(ptcl->pdgId()==25){
	            truth_H_pt = ptcl->pt()/1000.0;
	            truth_H_eta= ptcl->eta();
	            truth_H_phi= ptcl->phi();
	            truth_H_m  = ptcl->m()/1000.0;
	          }
	          if(ptcl->pdgId()==35){
	            truth_S_pt = ptcl->pt()/1000.0;
	            truth_S_eta= ptcl->eta();
	            truth_S_phi= ptcl->phi();
	            truth_S_m  = ptcl->m()/1000.0;
	          }
	
            // Loop over the truth X
	          int ip=0;
	          printf("  parent. ID:%5d, status: %2d, pt: %5f, eta: %5f, phi: %5f, mass: %5f\n",
	            ptcl->parent(ip)->pdgId(), ptcl->parent(ip)->status(), ptcl->parent(ip)->pt(), ptcl->parent(ip)->eta(), ptcl->parent(ip)->phi(), ptcl->parent(ip)->m());
	          // Truth information on the b-quarks from S and H for each X
            for (int ic=0;ic<nchild;++ic){
	            printf("  children. ID:%5d, status: %2d, pt: %5f, eta: %5f, phi: %5f, mass: %5f\n",
		            ptcl->child(ic)->pdgId(),ptcl->child(ic)->status(),ptcl->child(ic)->pt(),ptcl->child(ic)->eta(),ptcl->child(ic)->phi(),ptcl->child(ic)->m());
	            if(ptcl->pdgId()==25){
	              truth_b_fromH_pt.push_back(ptcl->child(ic)->pt()/1000.0);
	              truth_b_fromH_eta.push_back(ptcl->child(ic)->eta());
	              truth_b_fromH_phi.push_back(ptcl->child(ic)->phi());
	              truth_b_fromH_m.push_back(ptcl->child(ic)->m()/1000.0);
	            }
	            if(ptcl->pdgId()==35){
	              truth_b_fromS_pt.push_back(ptcl->child(ic)->pt()/1000.0);
	              truth_b_fromS_eta.push_back(ptcl->child(ic)->eta());
	              truth_b_fromS_phi.push_back(ptcl->child(ic)->phi());
	              truth_b_fromS_m.push_back(ptcl->child(ic)->m()/1000.0);
	            }
	          } 
          }
        }
      }
    } 
  }
  
  
  if(Helpers::check_sys("MET",sysname))
  {
   const xAOD::MissingETContainer* met(nullptr);
   ANA_CHECK (m_metHandle.retrieve (met, sys));
   
   met_pt.clear();
   met_px.clear();
   met_py.clear();
   met_sumet.clear();
   met_phi.clear();
   
   const xAOD::MissingET* final = (*met)["Final"];
   if(final)
   {
    met_pt.push_back(final->met()/1000.0);
    met_px.push_back(final->mpx()/1000.0);
    met_py.push_back(final->mpy()/1000.0);
    met_sumet.push_back(final->sumet()/1000.0);
    met_phi.push_back(final->phi());
   }
  }
  
  m_helpTree[sysname]->Fill();

 } //for sys list

 return StatusCode::SUCCESS;

}


StatusCode SH4BTree :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  
  
  if (!m_helpTree.empty())
  {
    for( auto tree : m_helpTree)
    {
      if (tree.second) delete tree.second;
    }
  }
  
  TFile* treeFile = wk()->getOutputFile( m_name.c_str() );
  
  return StatusCode::SUCCESS;
}


StatusCode SH4BTree::AddTree(string syst = "")
{
  
  Info("AddTree()", "%s", m_name.c_str() );
  
  string treeName = "MiniTree_";
  if (!syst.empty()) treeName += syst;
  TTree * outTree = new TTree(treeName.c_str(), treeName.c_str());
  if( !outTree )
  {
    Error("AddTree()","Failed to instantiate output tree!");
    return StatusCode::FAILURE;
  }
  
  // get the file we created already
  TFile* treeFile = wk()->getOutputFile( m_name.c_str() );
  outTree->SetDirectory( treeFile );
  
  m_helpTree[syst] = new HelpTreeBase( m_event, outTree, treeFile );
  
  if(Helpers::check_sys("EventInfo",syst))
   m_helpTree[syst]->AddEvent(m_evtDetailStr);

  if(!m_muonContainerName.empty() && Helpers::check_sys("Muons",syst))
    m_helpTree[syst]->AddMuons(m_muonDetailStr);

  if(!m_reco10JetContainerName.empty() && Helpers::check_sys("largeR",syst) && !m_isPhyslite)
    m_helpTree[syst]->AddFatJets(m_reco10JetDetailStr, m_reco10JetName);
  
  if(Helpers::check_sys("VRtrackjets",syst) && !m_isPhyslite)
  {
   outTree->Branch("VRtrackjet_pt",  &VRtrackjet_pt);
   outTree->Branch("VRtrackjet_eta", &VRtrackjet_eta);
   outTree->Branch("VRtrackjet_phi", &VRtrackjet_phi);
   outTree->Branch("VRtrackjet_m",   &VRtrackjet_m);
   outTree->Branch("VRtrackjet_btag77", &VRtrackjet_btag77);
  }
    
  if(Helpers::check_sys("smallR",syst))
  {
   outTree->Branch("nrecojet_antikt4", &nrecojet_antikt4);
   outTree->Branch("recojet_antikt4_pt",  &recojet_antikt4_pt);
   outTree->Branch("recojet_antikt4_eta", &recojet_antikt4_eta);
   outTree->Branch("recojet_antikt4_phi", &recojet_antikt4_phi);
   outTree->Branch("recojet_antikt4_m",   &recojet_antikt4_m);
   outTree->Branch("recojet_antikt4_btag77", &recojet_antikt4_btag77);
   outTree->Branch("nrecojet_antikt4_btag77", &nrecojet_antikt4_btag77);
   outTree->Branch("recojet_antikt4_btagscore", &recojet_antikt4_btagscore);
  }
  
  if(Helpers::check_sys("Truth",syst))
  {
   outTree->Branch("truthjet_antikt4_pt",  &truthjet_antikt4_pt);
   outTree->Branch("truthjet_antikt4_eta", &truthjet_antikt4_eta);
   outTree->Branch("truthjet_antikt4_phi", &truthjet_antikt4_phi);
   outTree->Branch("truthjet_antikt4_m",   &truthjet_antikt4_m);
   
   if(!m_isPhyslite)
   {
    outTree->Branch("truthjet_antikt10_pt",  &truthjet_antikt10_pt);
    outTree->Branch("truthjet_antikt10_eta", &truthjet_antikt10_eta);
    outTree->Branch("truthjet_antikt10_phi", &truthjet_antikt10_phi);
    outTree->Branch("truthjet_antikt10_m",   &truthjet_antikt10_m);
   }

   if(!m_truthParticleContainerName.empty() && m_isMC)
   {
    outTree->Branch("truth_b_fromH_pt", &truth_b_fromH_pt);
    outTree->Branch("truth_b_fromH_eta", &truth_b_fromH_eta);
    outTree->Branch("truth_b_fromH_phi", &truth_b_fromH_phi);
    outTree->Branch("truth_b_fromH_m"  , &truth_b_fromH_m);
    outTree->Branch("truth_b_fromS_pt" , &truth_b_fromS_pt);
    outTree->Branch("truth_b_fromS_eta", &truth_b_fromS_eta);
    outTree->Branch("truth_b_fromS_phi", &truth_b_fromS_phi);
    outTree->Branch("truth_b_fromS_m"  , &truth_b_fromS_m);
    outTree->Branch("truth_H_pt"  ,&truth_H_pt);
    outTree->Branch("truth_H_eta" ,&truth_H_eta);
    outTree->Branch("truth_H_phi" ,&truth_H_phi);
    outTree->Branch("truth_H_m"   ,&truth_H_m );
    outTree->Branch("truth_S_pt"  ,&truth_S_pt);
    outTree->Branch("truth_S_eta" ,&truth_S_eta);
    outTree->Branch("truth_S_phi" ,&truth_S_phi);
    outTree->Branch("truth_S_m"   ,&truth_S_m);
   }

  }
  
  if(Helpers::check_sys("MET",syst))
  {
   outTree->Branch("met_pt",    &met_pt);
   outTree->Branch("met_px",    &met_px);
   outTree->Branch("met_py",    &met_py);
   outTree->Branch("met_sumet", &met_sumet);
   outTree->Branch("met_phi",   &met_phi);
  }
  
  // SetAutoFlush to reduce memory use
  outTree->SetAutoFlush(-500000);

  return StatusCode::SUCCESS;
}

