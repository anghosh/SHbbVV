#include <sh4b/SH4BTree.h>
#include <sh4b/EventSelection.h>
#include <sh4b/Helpers.h>
//#include <XhhCommon/MinimalNtuple.h>
//#include <XhhCommon/XhhMiniNtuple.h>
//#include <XhhCommon/BJetTriggerEmulationAlg.h>
//#include <XhhCommon/EventSelection.h>
//#include <XhhCommon/DiHiggs.h>
//#include <XhhCommon/TrigMatching.h>
//#include <XhhCommon/TrigSF.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class SH4BTree+;
//#pragma link C++ class MinimalNtuple+;
//#pragma link C++ class XhhMiniNtuple+;
//#pragma link C++ class BJetTriggerEmulationAlg+;
#pragma link C++ class EventSelection+;
//#pragma link C++ class DiHiggs+;
//#pragma link C++ class TrigMatching+;
//#pragma link C++ class TrigSF+;

#endif
