#include <sh4b/EventSelection.h>
#include <xAODEventInfo/EventInfo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/AuxContainerBase.h"
#include <xAODJet/JetContainer.h>

using std::cout;  using std::endl;
using std::string; using std::vector;
#include <cstdlib>

#include <xAODAnaHelpers/HelpTreeBase.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

EventSelection :: EventSelection (const std::string& name,ISvcLocator *pSvcLocator) : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  
  declareProperty("m_name", m_name);
  declareProperty("m_outputStream", m_outputStream);
  declareProperty("m_muonContainerName", m_muonContainerName);
  declareProperty("m_muonPtCut", m_muonPtCut);
  declareProperty("m_muonEtaCut", m_muonEtaCut);
  declareProperty("m_reco4JetName", m_reco4JetName);
  declareProperty("m_reco10JetName",  m_reco10JetName);
  declareProperty("m_smallRjetPtCut",m_smallRjetPtCut);
  declareProperty("m_smallRjetNumberCut",m_smallRjetNumberCut);
  declareProperty("m_smallRbjetNumberCut",m_smallRbjetNumberCut);
  declareProperty("m_smallRjetEtaCut",m_smallRjetEtaCut);
  declareProperty("m_largeRjetPtCut",m_largeRjetPtCut);
  declareProperty("m_largeRjetNumberCut",m_largeRjetNumberCut);
  declareProperty("m_largeRjetTrackName",m_largeRjetTrackName);
  declareProperty("m_trackJetPtCut", m_trackJetPtCut);
  declareProperty("m_trackJetEtaCut", m_trackJetEtaCut);
  declareProperty("m_trigStudy",m_trigStudy);
  declareProperty("m_triggerChains",m_triggerChains);
 declareProperty("m_isPhyslite", m_isPhyslite=false);
}


StatusCode EventSelection :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  ANA_MSG_INFO ("in initialize");

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
 
  TFile* ofile = wk()->getOutputFile( m_outputStream );
  ANA_CHECK( evtStore()->event()->writeTo( ofile ) );
 
  m_debug=false;
 
  if(m_debug) Info("initialize()", "after add store");
  
  m_systematicsList.addHandle(m_muonHandle);
  m_systematicsList.addHandle(m_smallRJetHandle); 
  m_systematicsList.addHandle(m_largeRJetHandle);
  m_systematicsList.addHandle(m_muonWriteHandle);
  m_systematicsList.addHandle (m_smallRJetWriteHandle);
  m_systematicsList.addHandle (m_largeRJetWriteHandle);
  ANA_CHECK (m_systematicsList.initialize());

  // Trigger stuff
  m_tdec.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
  ANA_CHECK( m_tdec.initialize() );

  if(m_debug) Info("initialize()", "left");
   
  return StatusCode::SUCCESS;
}



StatusCode EventSelection :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));


  //-----------------------------
  // Primary Vertex 'quality' cut (copied from BasicEventSelection alg)
  //-----------------------------

  const xAOD::VertexContainer* vertices(nullptr);
  ANA_CHECK( HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store, msg()) );
  if ( !HelperFunctions::passPrimaryVertexSelection( vertices, 2 ) )
  {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
  }
  
  // print out run and event number from retrieved object
  //ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
 
  for (const auto& sys : m_systematicsList.systematicsVector())
  {
  	
    if (!m_muonContainerName.empty()) {
      const xAOD::MuonContainer* muons(nullptr);
      ANA_CHECK (m_muonHandle.retrieve (muons, sys));
      
      auto muonsOut = std::make_unique<xAOD::MuonContainer>();
      auto muonsOutAux = std::make_unique<xAOD::AuxContainerBase>();
      muonsOut->setStore (muonsOutAux.get()); //< Connect the two
      auto smallRbJetsOut = std::make_unique<xAOD::JetContainer>();
      auto smallRbJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
      smallRbJetsOut->setStore (smallRbJetsOutAux.get()); //< Connect the two
      
      for (const xAOD::Muon* muon : *muons) {
        if ((muon->pt() > m_muonPtCut) && (abs(muon->eta()) < m_muonEtaCut)) {
          xAOD::Muon* outMuon = new xAOD::Muon();
          muonsOut->push_back (outMuon); 
          *outMuon = *muon; 
        }
      }
      ANA_CHECK (m_muonWriteHandle.record (std::move (muonsOut), std::move (muonsOutAux), sys));
    }
    
    if (!m_reco4JetName.empty()) {
      // loop over the jets in the container
      const xAOD::JetContainer* smallRJets(nullptr);
      ANA_CHECK( m_smallRJetHandle.retrieve(smallRJets, sys));
      
      
      // Create the new container and its auxiliary store.
      auto smallRJetsOut = std::make_unique<xAOD::JetContainer>();
      auto smallRJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
      smallRJetsOut->setStore (smallRJetsOutAux.get()); //< Connect the two
      auto smallRbJetsOut = std::make_unique<xAOD::JetContainer>();
      auto smallRbJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
      smallRbJetsOut->setStore (smallRbJetsOutAux.get()); //< Connect the two
      for (const xAOD::Jet* jet : *smallRJets) {
        
        if ((jet->pt() > m_smallRjetPtCut) && (abs(jet->eta()) < m_smallRjetEtaCut))  {
          xAOD::Jet* outJet = new xAOD::Jet();
          xAOD::Jet* outbJet = new xAOD::Jet();
          *outJet = *jet; // copies auxdata from one auxstore to the other
          *outbJet = *jet; // copies auxdata from one auxstore to the other
          //ANA_MSG_INFO ("in execute, eventNumber = " << eventInfo->eventNumber() << " passes the cuts");
          //ANA_MSG_INFO ("Jet pt=" << jet->pt() << "     eta=" << jet->eta());
          // If there are muons in the container, apply muon in jet corrections
          if (!m_muonContainerName.empty()) {
            const xAOD::MuonContainer* muons(nullptr);
            ANA_CHECK (m_muonHandle.retrieve (muons, sys));
            ApplyMuonInSmallRJetCorrection(outJet, muons);
          }
          
          smallRJetsOut->push_back (outJet); // jet acquires the outJets auxstore
          smallRbJetsOut->push_back (outbJet); // b-jet acquires the outbJets auxstore
          
          //Change small R jet pt and mass from MeV to GeV
          double newPt =  outJet->pt() / (1000.0);
          double newMass =  outJet->m() / (1000.0);
          xAOD::JetFourMom_t newp4 (newPt, outJet->eta(), outJet->phi(), newMass);
          outJet->setJetP4 (newp4); // we've overwritten the 4-momentum
          SG::AuxElement::ConstAccessor<char> isTag77("ftag_select_DL1r_FixedCutBEff_77");
        //  if(isTag77.isAvailable(*outJet))
        //    outJet_btag77.push_back(isTag77(*outJet));
          if (isTag77.isAvailable(*outJet)){
            double newbPt =  outbJet->pt() / (1000.0);
            double newbMass =  outbJet->m() / (1000.0);
            xAOD::JetFourMom_t newbp4 (newbPt,outbJet->eta(), outbJet->phi(), newbMass);
            outbJet->setJetP4 (newbp4);
          }
        }
      }
            // Record the jets that pass the number and b-jrt number cuts into the event store or record empty container
      if (smallRJetsOut->size() >= m_smallRjetNumberCut && smallRbJetsOut->size() >= m_smallRbjetNumberCut) { 
        ANA_CHECK (m_smallRJetWriteHandle.record (std::move (smallRJetsOut), std::move (smallRJetsOutAux), sys));
      }
      else {
        auto emptyJetsOut = std::make_unique<xAOD::JetContainer>();
        auto emptyJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
        emptyJetsOut->setStore (emptyJetsOutAux.get()); //< Connect the two
        ANA_CHECK (m_smallRJetWriteHandle.record (std::move (emptyJetsOut), std::move (emptyJetsOutAux), sys));
      }
    }
    
    if (!m_reco10JetName.empty() && !m_isPhyslite)
    {
      // loop over the jets in the container
      const xAOD::JetContainer* largeRJets(nullptr);
      ANA_CHECK( m_largeRJetHandle.retrieve(largeRJets, sys));
     
     
      // Create the new container and its auxiliary store.
      auto largeRJetsOut = std::make_unique<xAOD::JetContainer>();
      auto largeRJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
      largeRJetsOut->setStore (largeRJetsOutAux.get()); //< Connect the two
     
      for (const xAOD::Jet* jet : *largeRJets) {
        xAOD::Jet* outJet = new xAOD::Jet();
        *outJet = *jet; // copies auxdata from one auxstore to the other
        
        // If there are muons in the container, apply muon in jet corrections
        if (!m_muonContainerName.empty()) {

          const xAOD::MuonContainer* muons(nullptr);
          ANA_CHECK (m_muonHandle.retrieve (muons, sys));
          
          // Link to parent for track jet association and load and sort track jets by pt
          ElementLink<xAOD::JetContainer> parentLink = jet->auxdata<ElementLink<xAOD::JetContainer> >("Parent");
          const xAOD::Jet* parent = *parentLink;
          vector<const xAOD::Jet*> assoTrackJets;
          try {
            assoTrackJets = parent->getAssociatedObjects<xAOD::Jet>(m_largeRjetTrackName);
          }
          catch (const std::exception& e) {
            ANA_MSG_ERROR ("Large R jet track name doesn't exist, trying with older name");
            assoTrackJets = parent->getAssociatedObjects<xAOD::Jet>("GhostVR30Rmax4Rmin02PV0TrackJet");
          }
          std::sort(assoTrackJets.begin(), assoTrackJets.end(), EventSelection::SortPt);
          
          // Select the track jets from the associated that pass the cuts
          std::vector<const xAOD::Jet*> selTrackJets;
          for(const xAOD::Jet* trackJet : assoTrackJets){
            if((trackJet->pt() > m_trackJetPtCut) && (abs(trackJet->eta()) < m_trackJetEtaCut) && (trackJet->numConstituents() > 1)) {
              selTrackJets.push_back(trackJet);
            }
          }

          ApplyMuonInLargeRJetCorrection(outJet, muons, selTrackJets);
        }
        
        if (outJet->pt() > m_largeRjetPtCut) {
          largeRJetsOut->push_back (outJet); // jet acquires the outJets auxstore
        }
        else
        	delete outJet;
      }
      //
      
      // Record the jets that pass the number cut into the event store or record empty container
      if (largeRJetsOut->size() >= m_largeRjetNumberCut) { 
        ANA_CHECK (m_largeRJetWriteHandle.record (std::move (largeRJetsOut), std::move (largeRJetsOutAux), sys));
      }
      else
      {
        auto emptyJetsOut = std::make_unique<xAOD::JetContainer>();
        auto emptyJetsOutAux = std::make_unique<xAOD::AuxContainerBase>();
        emptyJetsOut->setStore (emptyJetsOutAux.get()); //< Connect the two
        ANA_CHECK (m_largeRJetWriteHandle.record (std::move (emptyJetsOut), std::move (emptyJetsOutAux), sys));
      }  
    }
    
    
    if(m_trigStudy)
    {
     ANA_MSG_INFO ("Trigger selection ");
  	 // From John - to be changed
     std::string trig = "HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25";
     //	if(trig in m_triggerChains)
     ANA_MSG_INFO("Checking if trigger is passed");
     bool pass = m_tdec->isPassed(trig);
     ANA_MSG_INFO("  --> DONE! The decision of " << trig << " is: " << pass);		    
    }

  } //systematics loop
  return StatusCode::SUCCESS;
}


StatusCode EventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  
  
  ANA_MSG_INFO ("finalizing");
  
  TFile* ofile = wk()->getOutputFile( m_outputStream );
  ANA_CHECK( evtStore()->event()->finishWritingTo( ofile ) );
  
  return StatusCode::SUCCESS;
}

void EventSelection::ApplyMuonInSmallRJetCorrection(xAOD::Jet* jet, const xAOD::MuonContainer* muons) const{

  if(!jet || !muons) return;

  // Decide which muon to use (if any) for muon-in-jet correction
  float bestDR = 0.4;
  const xAOD::Muon* matchedMuon = 0;
  for(const xAOD::Muon* muon : *muons){
    float thisDR = jet->p4().DeltaR(muon->p4());
    if(thisDR < bestDR){
      matchedMuon = muon;
      bestDR = thisDR;
    }
  }

  // If we found one, add its 4-momentum (corrected for energy loss) to the jet
  if(matchedMuon){
    float eLoss = 0;
    if(!matchedMuon->parameter(eLoss, xAOD::Muon::EnergyLoss))
      Warning("ApplyResolvedMuonCorrection", "Muon energy loss not available!");
    TVector3 eLossVec;
    eLossVec.SetMagThetaPhi(eLoss, matchedMuon->p4().Theta(), matchedMuon->p4().Phi());
    TLorentzVector newP4 = jet->p4() + matchedMuon->p4() - TLorentzVector(eLossVec, eLoss);
    jet->setJetP4(xAOD::JetFourMom_t(newP4.Pt(), newP4.Eta(), newP4.Phi(), newP4.M()));
    //ANA_MSG_INFO ("Muon-in-jet correction applied.");
  }
}

void EventSelection::ApplyMuonInLargeRJetCorrection(xAOD::Jet* fatJet, const xAOD::MuonContainer* muons,
                                                std::vector<const xAOD::Jet*> trackJets) const{

  if(!fatJet || !muons) return;

  std::vector<const xAOD::Muon*> usedMuons;

  // Up to one muon used per track jet
  for(const xAOD::Jet* trackjet : trackJets){

    // Compute effective radius of this track jet (assume it's VR with these parameters)
    float bestDR = 30000./trackjet->pt();
    if(bestDR > 0.4 ) bestDR = 0.4;
    if(bestDR < 0.02) bestDR = 0.02;

    // Decide which muon to use, if any
    const xAOD::Muon* matchedMuon = 0;
    for(const xAOD::Muon* muon : *muons){

      // Guard against double-counting this muon if it falls within 2
      // overlapping track jets
      bool alreadyUsed = false;
      for(const xAOD::Muon* usedMu : usedMuons){
        if(muon == usedMu){
          alreadyUsed = true;
          break;
        }
      }
      if(alreadyUsed) break;

      float thisDR = trackjet->p4().DeltaR(muon->p4());
      if(thisDR < bestDR){
        matchedMuon = muon;
        bestDR = thisDR;
      }
    }

    // If we found one, add its 4-momentum (corrected for energy loss) to the jet
    if(matchedMuon){
      float eLoss = 0;
      if(!matchedMuon->parameter(eLoss, xAOD::Muon::EnergyLoss))
        Warning("execute()", "Muon energy loss not available!");
      TVector3 eLossVec;
      eLossVec.SetMagThetaPhi(eLoss, matchedMuon->p4().Theta(), matchedMuon->p4().Phi());
      TLorentzVector newP4 = fatJet->p4() + matchedMuon->p4() - TLorentzVector(eLossVec, eLoss);
      fatJet->setJetP4(xAOD::JetFourMom_t(newP4.Pt(), newP4.Eta(), newP4.Phi(), newP4.M()));
      usedMuons.push_back(matchedMuon);
      ANA_MSG_INFO ("Muon-in-jet correction applied to large R jets");
    }
  }
}

