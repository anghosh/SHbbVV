#!/bin/zsh
echo $ROOTSYS
export PROMPT='$ '
export RPROMPT=''

TIMESTAMP=`date -u +%Y-%m-%dT%H%MZ`
TAG=`git describe --tag`
local -a mc
local -a mode
local -a systematics
local -a minimal
local -a intermediate
local -a noboosted
local -a override
local -a boostoverride
local -a daodphys
local -a wgtsys
local -a nonresonant
local -a daodphyslite

# Parse options. Leading - indicates long option, : indicates argument
zparseopts -D -- -mc=mc -af2=af2 -minimal=minimal m:=mode -mode:=mode s:=systematics -systematics:=systematics -intermediate=intermediate -noboosted=noboosted -override=override -boostonlyoverride=boostonlyoverride -daodphys=daodphys -wgtsys=wgtsys -nonresonant=nonresonant -daodphyslite=daodphyslite

autoload compinit bashcompinit
compinit
bashcompinit

autoload colors && colors

function print_color() {
	echo -en "\e[$color[$1];1m"
	echo -n $2
	echo -en "\e[0m"
}

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# Select local (debugging) or grid mode
if (( !${+mode[1]} )); then
    echo "mode is required -- local (for testing), files, or grid"
    exit 1
fi

# Grid submission checklist
if (( !${+override[1]} )); then
if [[ "${mode[2]}" = "grid" ]]; then
    print_color blue "CHECKLIST FOR GRID SUBMISSION\n"
    print_color blue "-----------------------------\n"

    echo -n "1. Are there uncommitted changes? "
    output=$(git status --untracked-files=no --porcelain)
    if [[ -n "$output" ]]; then
        print_color red "YES\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    else
        print_color green "No\n"
    fi
    option_in=
    vared -p "2. Have you run a local test [y/N]? " option_in
    if [[ $option_in =~ '^[Yy].*$' ]]; then
    else
        print_color red "Please run a local test before submitting\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    fi

    echo -n "3. Are we on the master branch? "
    output=$(git rev-parse --abbrev-ref HEAD)
    if [[ $output != "master" ]]; then
        print_color red "NO\n"
        option_in=
        vared -p "   Override [y/N]? " option_in
        if [[ $option_in =~ '^[Yy].*$' ]]; then
            print_color green "   OK, continuing.\n"
        else
            print_color red "Checklist failed -- quitting\n"
            exit 2
        fi
    else
        print_color green "Yes\n"
    fi

    echo -n "4. Are we on a tag? "
    output=$(git describe --exact-match HEAD 2>/dev/null)
    if [[ -z "$output" ]]; then
        print_color red "NO\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    else
        print_color green "Yes\n"
    fi

    echo -n "5. Does it match the AnalysisBase version? "
    if [[ "$output" = "$AnalysisBase_VERSION"* ]]; then
        print_color green "Yes\n"
    else
        print_color red "NO\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    fi
    print_color green "Checklist succeeded -- continuing\n\n"
fi
else
    print_color yellow "CHECKLIST OVERRIDEN\n\n"
fi

# eo is the extra options flag
eo=""
suffix="full"
if (( ${+minimal[1]} )); then
    eo="--minimalBranches"
    suffix="min"
fi

# Set the top level configuration
jo="../sources/XhhCommon/config/resonantConfig.py"

if (( ${+nonresonant[1]} )); then
    jo="../sources/XhhCommon/config/nonresonantConfig.py"
    suffix="${suffix}_nonres"
else
    # Select variable radius(VR) boosted trackjets
    eo="$eo --doVR"

    # Resonant-specific options
    if (( ${+intermediate[1]} )); then
        eo="$eo --doIntermediate"
        suffix="${suffix}_inter"
    fi

    if (( ${+noboosted[1]} )); then
        eo="$eo --noBoosted"
        suffix="${suffix}_noBst"
    fi

    if (( ${+boostonlyoverride[1]} )); then
        eo="$eo --doBoostOverride"
        suffix="${suffix}_BstOnly"
    fi
fi

if (( ${+daodphys[1]} )); then
    eo="$eo --daodPhys"
fi

if (( ${+wgtsys[1]} )); then
    eo="$eo --doWgtSys"
fi

if (( ${+daodphyslite[1]} )); then
    eo="$eo --daodPhyslite"
fi

# Include systematics
if (( ${+systematics[1]} && !${+mc[1]} )); then
    echo "Systematics should only be run on MC"
    exit 1
fi

if (( ${+systematics[1]} && ${+mc[1]} )); then
    syst_lab=""
    if [[ ${systematics[2]} = "boosted" ]]; then
        if (( ${+af2[1]} )); then
            echo "The boosted analysis is not supported for AFII samples"
            exit 1
        fi
        if (( ${+nonresonant[1]} )); then
            echo "The boosted channel is not supported for nonresonant"
            exit 1
        fi
        eo="${eo} --doBoostedSys"
        syst_lab="-bst"
    elif [[ ${systematics[2]} = "resolved" ]]; then
        if (( ${+nonresonant[1]} )); then
            eo="${eo} --doSystematics"
        else
            eo="${eo} --doResolvedSys"
            if (( !${+af2[1]} )); then
                syst_lab="-res"
            fi
        fi
    elif [[ ${systematics[2]} = "both" ]]; then
        if (( ${+nonresonant[1]} )); then
            echo "The boosted channel is not supported for nonresonant -- running resolved systs only"
            eo="${eo} --doSystematics"
        elif (( ${+af2[1]} )); then
            echo "The boosted analysis is not supported for AFII samples -- running resolved systs only"
            eo="${eo} --doResolvedSys"
        else
            eo="${eo} --doResolvedSys --doBoostedSys"
        fi
    else
        echo "Invalid systematic type"
        exit 1
    fi
    suffix="${suffix}_sys${syst_lab}"
fi

# Is sample MC
if (( ${+mc[1]} )); then
    is_mc="--isMC"
    # Is sample AFII
    if (( ${+af2[1]} )); then
        is_af2="--isAFII"
    else
        is_af2=""
    fi
else
    is_mc=""
    is_af2=""
fi


if [[ "${mode[2]}" != "files" ]]; then
    voms-proxy-info -e
    if (( $?==1 )); then
        echo "VOMS Proxy Needed"
        exit 1
    fi
    lsetup rucio
    lsetup fax
fi

# Choose appropriate driver
if [[ "${mode[2]}" = "grid" ]]; then
  driver="prun"
  localSetupPandaClient
  inputType="--inputRucio --inputList"
elif [[ "${mode[2]}" = "local" ]]; then
  #driver="--nevents=1000 direct"
  driver="--nevents=-1 direct"
  inputType="--inputRucio --inputList"
  driver_opt=""
elif [[ "${mode[2]}" = "files" ]]; then
  driver="direct"
  inputType=""
  driver_opt=""
else
  echo "mode must be local (for testing), files, or grid (got ${mode[2]})"
  exit 1
fi

# Run
for arg in $@
do
    if [[ "${mode[2]}" = "grid" ]]; then
      #grab the filename from the .txt file, use this for a title in the outputs
      filename=$(basename -- "$arg")
      group="${filename%.*}"
      driver_opt=(--optGridOutputSampleName="user.%nickname%.HH4B.%in:name[2]%.$group.%in:vers%.AB$TAG.${suffix}")
      if (( ${+systematics[1]} && ${+mc[1]} )); then
          driver_opt=(${driver_opt} --optGridNGBPerJob=10)
      fi
    else
        echo "Suffix would be ${suffix} (${#suffix} characters)"
    fi
    xAH_run.py --files $arg ${(z)inputType} --config ${jo} --extraOptions=${eo} --submitDir ./XhhCommon-${arg:t:r}-${suffix}-$TAG --inputTag '*DAOD*' $is_mc $is_af2 --force ${(z)driver} ${driver_opt}
done

