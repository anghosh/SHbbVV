#!/bin/zsh
echo $ROOTSYS
export PROMPT='$ '
export RPROMPT=''

TIMESTAMP=`date -u +%Y-%m-%dT%H%MZ`
TAG=`git describe --tag`
local -a mc
local -a mode
local -a daodphys
local -a daodphyslite

# Parse options. Leading - indicates long option, : indicates argument
zparseopts -D -- -mc=mc m:=mode -mode:=mode -daodphys=daodphys -daodphyslite=daodphyslite

autoload compinit bashcompinit
compinit
bashcompinit

autoload colors && colors

function print_color() {
	echo -en "\e[$color[$1];1m"
	echo -n $2
	echo -en "\e[0m"
}

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# Select local (debugging) or grid mode
if (( !${+mode[1]} )); then
    echo "mode is required -- local (for testing), files, or grid"
    exit 1
fi

# Grid submission checklist
if (( !${+override[1]} )); then
if [[ "${mode[2]}" = "grid" ]]; then
    print_color blue "CHECKLIST FOR GRID SUBMISSION\n"
    print_color blue "-----------------------------\n"

    echo -n "1. Are there uncommitted changes? "
    output=$(git status --untracked-files=no --porcelain)
    if [[ -n "$output" ]]; then
        print_color red "YES\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    else
        print_color green "No\n"
    fi
    option_in=
    vared -p "2. Have you run a local test [y/N]? " option_in
    if [[ $option_in =~ '^[Yy].*$' ]]; then
    else
        print_color red "Please run a local test before submitting\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    fi

    echo -n "3. Are we on the master branch? "
    output=$(git rev-parse --abbrev-ref HEAD)
    if [[ $output != "master" ]]; then
        print_color red "NO\n"
        option_in=
        vared -p "   Override [y/N]? " option_in
        if [[ $option_in =~ '^[Yy].*$' ]]; then
            print_color green "   OK, continuing.\n"
        else
            print_color red "Checklist failed -- quitting\n"
            exit 2
        fi
    else
        print_color green "Yes\n"
    fi

    echo -n "4. Are we on a tag? "
    output=$(git describe --exact-match HEAD 2>/dev/null)
    if [[ -z "$output" ]]; then
        print_color red "NO\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    else
        print_color green "Yes\n"
    fi

    echo -n "5. Does it match the AnalysisBase version? "
    if [[ "$output" = "$AnalysisBase_VERSION"* ]]; then
        print_color green "Yes\n"
    else
        print_color red "NO\n"
        print_color red "Checklist failed -- quitting\n"
        exit 2
    fi
    print_color green "Checklist succeeded -- continuing\n\n"
fi
else
    print_color yellow "CHECKLIST OVERRIDEN\n\n"
fi

# eo is the extra options flag
eo=""
suffix="full"
if (( ${+minimal[1]} )); then
    eo="--minimalBranches"
    suffix="min"
fi

# Set the top level configuration
jo="../sources/XhhCommon/config/minimal_commonCP.py"

if (( ${+daodphys[1]} )); then
    eo="$eo --daodPhys"
fi

if (( ${+daodphyslite[1]} )); then
    eo="$eo --daodPhyslite"
fi


# Is sample MC
if (( ${+mc[1]} )); then
    is_mc="--isMC"
else
    is_mc=""
fi

if [[ "${mode[2]}" != "files" ]]; then
    voms-proxy-info -e
    if (( $?==1 )); then
        echo "VOMS Proxy Needed"
        exit 1
    fi
    lsetup rucio
    lsetup fax
fi

# Choose appropriate driver
if [[ "${mode[2]}" = "grid" ]]; then
  driver="prun"
  localSetupPandaClient
  inputType="--inputRucio --inputList"
elif [[ "${mode[2]}" = "local" ]]; then
  #driver="--nevents=100 direct"
  driver="--nevents=-1 direct"
  inputType="--inputRucio --inputList"
  driver_opt=""
elif [[ "${mode[2]}" = "files" ]]; then
  driver="direct"
  inputType=""
  driver_opt=""
else
  echo "mode must be local (for testing), files, or grid (got ${mode[2]})"
  exit 1
fi

# Run
for arg in $@
do
    if [[ "${mode[2]}" = "grid" ]]; then
      #grab the filename from the .txt file, use this for a title in the outputs
      filename=$(basename -- "$arg")
      group="${filename%.*}"
      driver_opt=(--optGridOutputSampleName="user.%nickname%.HH4B.%in:name[2]%.$group.%in:vers%.AB$TAG.${suffix}")
      if (( ${+systematics[1]} && ${+mc[1]} )); then
          driver_opt=(${driver_opt} --optGridNGBPerJob=10)
      fi
    else
        echo "Suffix would be ${suffix} (${#suffix} characters)"
    fi
    #xAH_simple_lite.py --files $arg ${(z)inputType} --nevents=-1 --config ${jo} --extraOptions=${eo} --submitDir ./XhhCommon-${arg:t:r}-NOMINAL --inputTag '*DAOD*' $is_mc --force ${(z)driver} ${driver_opt} 
    xAH_simple_lite.py --files $arg ${(z)inputType} --nevents=3000 --config ${jo} --extraOptions=${eo} --submitDir ./XhhCommon-${arg:t:r} --inputTag '*DAOD*' $is_mc --force ${(z)driver} ${driver_opt}
done

